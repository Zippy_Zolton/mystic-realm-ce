//Mystic Realm Shrine Lua
//By YumeDragoon

local shrine_active = false
local soffset = 220
local soffsetresettimer = 0
local statushasOpened = false
local currentmap = nil
local posx = 320
local mysticticker = 0

addHook("NetVars", function(net)
	shrine_active=net($)
	currentmap=net($)
	mysticticker=net($)
end)


addHook("MapLoad", function(p, v)
	for player in players.iterate
		if mapheaderinfo[gamemap].shrinecolor != nil //Does this map even have a shrine?
		and shrine_active == true //Has the player activated the shrine yet?
		and currentmap == gamemap //Is the player still on the level they activated the shrine on?
			P_LinedefExecute(5000, player.mo, 5001) //All is good so re-activate the shrine. Don't want to lose that progress!
		end
	end
	if mapheaderinfo[gamemap].shrinecolor == nil or currentmap != gamemap //Either this map has no shrine or the player has changed maps.
		shrine_active = false //NO FREE SHRINES! YOU PAY!
	end
end)

local function shrine_pressed(line, mo, d) //This is where we actually activate the shrine.
	for player in players.iterate
		if shrine_active == false //The player hasn't activated the shrine yet. let's do that now.
			currentmap = gamemap //Sets currentmap to the map number
			shrine_active = true
			mysticticker = 0
			S_StartSound(player.mo, sfx_kc5c)
		end
	end
	for p in players.iterate
		p.schangeoffset = 1 //Trigger bringing up the shrine marker.
	end
	soffsetresettimer = 95 //This makes the shrine marker stay up on screen for a bit longer
end

addHook("LinedefExecute", shrine_pressed, "SHRINE_PRESSED") //When the player steps on the button activate the shrine.

addHook("ThinkFrame", function() //This is the script that manages the positioning of the marker.
	for p in players.iterate
		if not p.bot and p.valid
			if statushasOpened == true //is the player holding tab?
				p.schangeoffset = 1
				soffsetresettimer = 2
				statushasOpened = false //make sure that letting go of tab dismisses the marker.
			end
			if p.schangeoffset
				if soffset > 155 //our y is greater than 155.
					soffset = $ - 5 //We gotta lower that. bring the marker up.
				end
				if soffset == 155 and soffsetresettimer //our y is equal to 155
					soffsetresettimer = $ - 1 //let's count down the timer until it auto dismisses itself
				end
				if not soffsetresettimer
					p.schangeoffset = 0
				end
			else
				if soffset < 220 //Our y is less than 220. Usually means the marker is on screen.
					soffset = $ + 10 //Let's increase it. Lowering the marker!
				end
			end
		end
	end
end)

hud.add(function(d, p) //The shrine marker draw code
	local sloffset = soffset //why is this like this? because we can't mess with soffset. it's needed to handle the animation timer.
	if (shrine_active == true)
		d.drawScaled(165*(FRACUNIT/2+FRACUNIT/3), (sloffset*FRACUNIT), (FRACUNIT/2)+FRACUNIT/3, d.cachePatch("MSMRK1"), V_SNAPTOBOTTOM, d.getColormap(TC_DEFAULT, mapheaderinfo[gamemap].shrinecolor))
		if mysticticker == 120
			return
		else
			mysticticker = $ + 1
		end
	else
		d.drawScaled(165*(FRACUNIT/2+FRACUNIT/3), (sloffset*FRACUNIT), (FRACUNIT/2)+FRACUNIT/3, d.cachePatch("MSMRK2"), V_SNAPTOBOTTOM, d.getColormap(TC_DEFAULT, SKINCOLOR_GREY))
	end
	if mysticticker == 120 or shrine_active == false
		return
	else
		d.drawString(posx, 105, mapheaderinfo[gamemap].lvlttl.."'s Mystic Shrine", V_ALLOWLOWERCASE, "center")
		d.drawString(posx, 115, "has been activated.", V_ALLOWLOWERCASE, "center")
		d.drawString(posx, 135, "The next level has been changed to", V_ALLOWLOWERCASE, "center")
		d.drawString(posx, 145, "-- "..mapheaderinfo[gamemap].eml.." --", V_ALLOWLOWERCASE, "center")
	end
	if mysticticker == 1
		posx = 320
	end
	if mysticticker >= 108
	and mysticticker >= 18
		posx = posx - 25
	end	
	if posx > 160 
		if mysticticker <= 9 
			posx = posx - 16
		elseif mysticticker <= 16
			posx = posx - 2
		elseif mysticticker <= 18
			posx = posx - 1
		end
	end
end, "game")

hud.add(function(v, p) //sets the stuff so that holding tab shows the emblem marker.
	statushasOpened = true
	soffsetresettimer = 2
end, "scores")