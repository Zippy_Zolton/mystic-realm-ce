--thanks spec for this lua, youre amazing

freeslot("MT_ICEBEAM", "MT_ICEBEAMSHARD", "MT_ICEBEAMPARTICLE", "S_ICEBEAMPARTICLE")

local function IceTouchesSomething(special, toucher)
	if special and special.valid and toucher and toucher.valid and toucher.player and toucher.player.frozen ~= 1
		toucher.player.frozen = 1
		toucher.player.frozentimer = 140
		toucher.player.oldcolor = toucher.color
	end
end

addHook("TouchSpecial", IceTouchesSomething, MT_ICEBEAM)
addHook("TouchSpecial", IceTouchesSomething, MT_ICEBEAMSHARD)

addHook("PlayerThink", function(p)
	p.frozen = $ or 0
	p.frozentimer = $ or 0
	p.oldcolor = $ or 0
	if p.frozen == 1 and p.frozentimer > 0
		p.mo.colorized = true
		p.mo.color = SKINCOLOR_ICY
		p.frozentimer = $-1
		p.pflags = $1|PF_FULLSTASIS
		P_SpawnMobjFromMobj(p.mo, (P_RandomRange(-5, 5) * FRACUNIT), (P_RandomRange(-5, 5) * FRACUNIT), ((p.mo.height / 2) + ((P_RandomRange(-3, 3) * FRACUNIT))), MT_ICEBEAMPARTICLE)
		if P_IsObjectOnGround(p.mo) and p.speed == 0
			p.mo.flags = $1|MF_NOTHINK
		end
		for mobj in mobjs.iterate()
			if mobj.type == MT_TAILSOVERLAY and mobj.tracer == p.mo
				mobj.flags = $1|MF_NOTHINK
			end
		end
	elseif p.oldcolor
		p.mo.colorized = false
		p.mo.color = p.oldcolor
		p.oldcolor = 0
		p.frozen = 0
		p.pflags = $1 & ~PF_FULLSTASIS
		p.mo.flags = $1 & ~MF_NOTHINK
		for mobj in mobjs.iterate()
			if mobj.type == MT_TAILSOVERLAY and mobj.tracer == p.mo
				mobj.flags = $1 & ~MF_NOTHINK
			end
		end
	end
end)

addHook("MobjDeath", function(mo)
	if mo.player and mo.player.oldcolor
		local p = mo.player
		p.frozen = 0
		p.frozentimer = 0
		mo.color = p.oldcolor
		p.pflags = $1 & ~PF_FULLSTASIS
		mo.flags = $1 & ~MF_NOTHINK
	end
end,MT_PLAYER)