//Xian.exe was here

local sonicx = 0
local sonicy = 0
local sonicz = 0

addHook("PlayerThink", function(p)
	if p.mo and p.mo.valid
		sonicx = p.mo.x
		sonicy = p.mo.y
		sonicz = p.mo.z
	end
end)

addHook("PlayerThink", function(p)
	if gamemap == 123 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD1) != 1)
		P_SpawnMobj(p.mo.x, p.mo.y, p.mo.z, MT_EMERALD1)
	elseif gamemap == 124 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD2) != 2)
		P_SpawnMobj(p.mo.x, p.mo.y, p.mo.z, MT_EMERALD2)
	elseif gamemap == 125 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD3) != 4)
		P_SpawnMobj(p.mo.x, p.mo.y, p.mo.z, MT_EMERALD3)
	elseif gamemap == 126 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD4) != 8)
		P_SpawnMobj(p.mo.x, p.mo.y, p.mo.z, MT_EMERALD4)
	elseif gamemap == 127 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD5) != 16)
		P_SpawnMobj(p.mo.x, p.mo.y, p.mo.z, MT_EMERALD5)
	elseif gamemap == 128 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD6) != 32)
		P_SpawnMobj(p.mo.x, p.mo.y, p.mo.z, MT_EMERALD6)
	elseif gamemap == 129 and (p.pflags & PF_FINISHED) and ((emeralds & EMERALD7) != 64)
		P_SpawnMobj(p.mo.x, p.mo.y, p.mo.z, MT_EMERALD7)
	end
end)

local function TeleportEmerald(mo)
	if not (gamemap >= 130 or gamemap <= 122)
		P_TeleportMove(mo, sonicx, sonicy, sonicz)
	end
end

addHook("MobjThinker", TeleportEmerald, MT_EMERALD1)
addHook("MobjThinker", TeleportEmerald, MT_EMERALD2)
addHook("MobjThinker", TeleportEmerald, MT_EMERALD3)
addHook("MobjThinker", TeleportEmerald, MT_EMERALD4)
addHook("MobjThinker", TeleportEmerald, MT_EMERALD5)
addHook("MobjThinker", TeleportEmerald, MT_EMERALD6)
addHook("MobjThinker", TeleportEmerald, MT_EMERALD7)
